#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "platform_kiki_container/platform_kiki_container.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
